" the usual goodies
set shiftwidth=4
set tabstop=4
set expandtab


" colours config
colorscheme dogrun 

" for rust syntax highlighting
syntax enable
filetype plugin indent on

" yaml files are annoying
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
