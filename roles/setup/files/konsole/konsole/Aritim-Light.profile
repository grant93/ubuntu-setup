[Appearance]
BoldIntense=true
ColorScheme=Aritim Light
Font=Hack,10,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[General]
Name=Aritim-Light_DEV
Parent=FALLBACK/

[Scrolling]
HistorySize=10000
ScrollBarPosition=1

[Terminal Features]
BlinkingCursorEnabled=true
